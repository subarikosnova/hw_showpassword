document.addEventListener("DOMContentLoaded", function() {
    const showPasswordIcons = document.querySelectorAll(".icon-password");

    showPasswordIcons.forEach(icon => {
        icon.addEventListener("click", () => {
            const input = icon.previousElementSibling;
            const isPasswordFieldVisible = input.type === "text";

            if (isPasswordFieldVisible) {
                input.type = "password";
                icon.classList.remove("fa-eye-slash");
                icon.nextElementSibling.style.display = "none";
            } else {
                input.type = "text";
                icon.classList.add("fa-eye-slash");
                icon.nextElementSibling.style.display = "inline-block";
            }
        });
    });

    const submitButton = document.getElementById("submitButton");
    const errorMessage = document.getElementById("errorMessage");

    submitButton.addEventListener("click", function() {
        const passwordInput = document.getElementById("passwordInput");
        const confirmPasswordInput = document.getElementById("confirmPasswordInput");

        const passwordValue = passwordInput.value;
        const confirmPasswordValue = confirmPasswordInput.value;

        if (passwordValue === confirmPasswordValue) {
            alert("You are welcome");
            errorMessage.textContent = "";
        } else {
            errorMessage.textContent = "Потрібно ввести однакові значення";
        }
    });
});
